#!/bin/bash

task=$1
service=$2

print_man() {
    printf "\nUsage:  run_app.sh TASK SERVICE\n\n"
    printf "A tool to easily invoke docker-compose commands\n\n"
    printf "Tasks:\n"
    printf "  build, b\tbuild the specified service\n"
    printf "  run, r\trun the previously built specified service\n"
    printf "  logs, l\tfollow the logs produced by the specified service\n"
    printf "  stop, s\tstop the specified service\n"
    printf "\nService:\n"
    printf "  all, a\tboth the PostgreSQL DB server and the GraphQL API\n"
    printf "  pg_db, p\tthe PostgreSQL DB server alone\n"
    printf "  gql_api, g\tthe GraphQL API alone\n"
}

run_command() {
    eval $1
}

exec_task() {
    case $1 in
    build | b)
        run_command "$2"
        ;;

    run | r)
        run_command "$3"
        ;;

    logs | l)
        run_command "$4"
        ;;

    stop | s)
        run_command "$5"
        ;;

    *)
        print_man
        ;;
    esac
}

exec_task_for_all() {
    exec_task $task \
        "echo 'building ALL services'
            && docker-compose --env-file .env.dev up --build -d" \
        "echo 'running ALL services'
            && docker-compose --env-file .env.dev up -d" \
        "echo 'following logs of ALL services'
            && docker-compose --env-file .env.dev logs -ft --tail=\"all\"" \
        "echo 'stopping ALL services'
            && docker-compose --env-file .env.dev down"
}

exec_task_for_service() {
    exec_task $task \
        "echo 'building service $service'
            && docker-compose --env-file .env.dev up --build -d $service" \
        "echo 'running service $service'
            && docker-compose --env-file .env.dev up -d $service" \
        "echo 'following logs of service $service'
            && docker-compose --env-file .env.dev logs -ft --tail="all" $service" \
        "echo 'stopping service $service'
            && docker-compose --env-file .env.dev rm -f -s -v $service"
}

exec_task_for_either() {
    case $service in
    pg_db | p)
        service="pg_db"
        exec_task_for_service
        ;;

    gql_api | g)
        service="gql_api"
        exec_task_for_service
        ;;

    *)
        print_man
        ;;

    esac
}

if [[ -n $task && -z $service ]]; then
    exec_task_for_all $task
elif [[ -z $task || -z $service ]]; then
    print_man
else
    if [ $service = "all" ] || [ $service = "a" ]; then
        exec_task_for_all
    else
        exec_task_for_either
    fi
fi
