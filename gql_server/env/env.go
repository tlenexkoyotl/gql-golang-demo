package env

import "os"

func GetEnv(varName, defaultValue string) string {
	value := os.Getenv(varName)

	if value == "" {
		value = defaultValue
	}

	return value
}
