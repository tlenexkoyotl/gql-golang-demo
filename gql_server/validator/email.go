package validator

import "regexp"

var emailRegexp = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")

func (v *Validator) IsEmail(field, email string) bool {
	if !v.FieldHasErrors(field) {
		return false
	}

	if !emailRegexp.MatchString(email) {
		v.Errors[field] = "Not a valid email"

		return false
	}

	return true
}
