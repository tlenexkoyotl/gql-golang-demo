package validator

import "fmt"

func (v *Validator) EqualToField(field string, value interface{}, toEqualField string, toEqualValue interface{}) bool {
	if !v.FieldHasErrors(field) {
		return false
	}

	if value != toEqualValue {
		v.Errors[field] = fmt.Sprintf("%s must equal %s: %v != %v", field, toEqualField, value, toEqualValue)

		return false
	}

	return true
}
