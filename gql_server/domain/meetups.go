package domain

import (
	"context"
	"errors"
	"fmt"

	"gitlab.com/tlenexkoyotl/meetmeup/auth_middleware"
	"gitlab.com/tlenexkoyotl/meetmeup/graph/model"
)

func (d *Domain) CreateMeetup(ctx context.Context, input model.NewMeetup) (*model.Meetup, error) {
	currentUser, err := auth_middleware.GetCurrentUserFromCtx(ctx)

	if err != nil {
		return nil, ErrUnauthenticated
	}

	if len(input.Name) < 1 {
		return nil, errors.New("Name is too short")
	}

	if len(input.Description) < 3 {
		return nil, errors.New("Description is too short")
	}

	meetup := &model.Meetup{
		Name:        input.Name,
		Description: input.Description,
		UserID:      currentUser.ID,
	}

	return d.MeetupRepo.CreateMeetup(meetup)
}

func (d *Domain) UpdateMeetup(ctx context.Context, id string, input model.UpdateMeetup) (*model.Meetup, error) {
	currentUser, err := auth_middleware.GetCurrentUserFromCtx(ctx)

	if err != nil {
		return nil, ErrUnauthenticated
	}

	meetup, err := d.MeetupRepo.GetByID(id)

	if err != nil || meetup == nil {
		return nil, errors.New("Meetup does not exist")
	}

	if !meetup.IsOwner(currentUser) {
		return nil, ErrForbidden
	}

	updateIsValid := false

	if input.Name != nil {
		if len(*input.Name) < 1 {
			return nil, errors.New("Meetup name is not long enough")
		}

		meetup.Name = *input.Name
		updateIsValid = true
	}

	if input.Description != nil {
		if len(*input.Description) < 1 {
			return nil, errors.New("Meetup description is not long enough")
		}

		meetup.Description = *input.Description
		updateIsValid = true
	}

	if !updateIsValid {
		return nil, errors.New("No update done")
	}

	meetup, err = d.MeetupRepo.UpdateMeetup(meetup)

	if err != nil {
		return nil, fmt.Errorf("Error while updating Meetup: %v", err)
	}

	return meetup, err
}

func (d *Domain) DeleteMeetup(ctx context.Context, id string) (bool, error) {
	currentUser, err := auth_middleware.GetCurrentUserFromCtx(ctx)

	if err != nil {
		return false, ErrUnauthenticated
	}

	meetup, err := d.MeetupRepo.GetByID(id)

	if err != nil || meetup == nil {
		return false, errors.New("Meetup does not exist")
	}

	if !meetup.IsOwner(currentUser) {
		return false, ErrForbidden
	}

	err = d.MeetupRepo.DeleteMeetup(meetup)

	if err != nil {
		return false, errors.New("Error while deleting meetup")
	}

	return true, nil
}
