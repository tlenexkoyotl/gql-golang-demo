package domain

import (
	"context"
	"errors"
	"fmt"
	"log"

	"gitlab.com/tlenexkoyotl/meetmeup/graph/model"
)

func (d *Domain) Register(ctx context.Context, input model.RegisterInput) (*model.AuthResponse, error) {
	_, err := d.UserRepo.GetUserByEmail(input.Email)

	if err == nil {
		return nil, errors.New(fmt.Sprintf("Email %v is already in use", input.Email))
	}

	_, err = d.UserRepo.GetUserByUsername(input.Username)

	if err == nil {
		return nil, errors.New(fmt.Sprintf("Username %v is already in use", input.Username))
	}

	if input.Password != input.ConfirmPassword {
		return nil, errors.New("Passwords do not match")
	}

	user := &model.User{
		Username:  input.Username,
		Email:     input.Email,
		FirstName: input.FirstName,
		LastName:  input.LastName,
	}

	err = user.HashPassword(input.Password)

	if err != nil {
		log.Printf("Error while hashing password: %v", err)

		return nil, errors.New("Something went wrong :(")
	}

	// TODO: create verification code

	tx, err := d.UserRepo.DB.Begin()

	if err != nil {
		log.Printf("Error while creating a transaction: %v", err)

		return nil, errors.New("Something went wrong :(")
	}

	defer tx.Rollback()

	if _, err := d.UserRepo.CreateUser(tx, user); err != nil {
		log.Printf("Error while creating a user: %v", err)

		return nil, err
	}

	if err = tx.Commit(); err != nil {
		log.Printf("Error while committing a transaction: %v", err)

		return nil, err
	}

	token, err := user.GenToken()

	if err != nil {
		log.Printf("Error while generating the token: %v", err)

		return nil, errors.New("Something went wrong :(")
	}

	authResponse := &model.AuthResponse{
		AuthToken: token,
		User:      user,
	}

	return authResponse, nil
}

func (d *Domain) Login(ctx context.Context, input model.LoginInput) (*model.AuthResponse, error) {
	user, err := d.UserRepo.GetUserByEmail(input.Email)

	if err != nil {
		return nil, ErrBadCredentials
	}

	err = user.ComparePassword(input.Password)

	if err != nil {
		return nil, ErrBadCredentials
	}

	token, err := user.GenToken()

	if err != nil {
		return nil, errors.New("Something went wrong :(")
	}

	authResponse := &model.AuthResponse{
		AuthToken: token,
		User:      user,
	}

	return authResponse, nil
}
