package domain

import (
	"errors"

	"gitlab.com/tlenexkoyotl/meetmeup/postgres"
)

var (
	ErrBadCredentials  = errors.New("Incorrect email/password combination")
	ErrUnauthenticated = errors.New("User unauthenticated")
	ErrForbidden       = errors.New("Unauthorized")
)

type Domain struct {
	MeetupRepo postgres.MeetupRepo
	UserRepo   postgres.UserRepo
}

func NewDomain(userRepo postgres.UserRepo, meetupRepo postgres.MeetupRepo) *Domain {
	return &Domain{UserRepo: userRepo, MeetupRepo: meetupRepo}
}
