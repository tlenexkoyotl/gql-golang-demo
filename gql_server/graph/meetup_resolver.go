package graph

import (
	"context"

	"gitlab.com/tlenexkoyotl/meetmeup/graph/generated"
	"gitlab.com/tlenexkoyotl/meetmeup/graph/model"
)

type meetupResolver struct{ *Resolver }

func (r *Resolver) Meetup() generated.MeetupResolver { return &meetupResolver{r} }

func (r *meetupResolver) User(ctx context.Context, obj *model.Meetup) (*model.User, error) {
	return getUserLoader(ctx).Load(obj.UserID)
}
