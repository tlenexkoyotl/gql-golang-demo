package model

import "gitlab.com/tlenexkoyotl/meetmeup/validator"

func (i RegisterInput) Validate() (bool, map[string]string) {
	v := validator.New()

	v.Required("email", i.Email)
	v.IsEmail("email", i.Email)

	v.Required("password", i.Password)
	v.MinLength("password", i.Password, 6)

	v.Required("confirmPassword", i.ConfirmPassword)
	v.EqualToField("confirmPassword", i.ConfirmPassword, "password", i.Password)

	v.Required("username", i.Username)
	v.MinLength("username", i.Username, 2)

	v.Required("firstname", i.FirstName)
	v.MinLength("firstname", i.FirstName, 2)

	v.Required("lastname", i.LastName)
	v.MinLength("lastname", i.LastName, 2)

	return v.IsValid(), v.Errors
}

func (i LoginInput) Validate() (bool, map[string]string) {
	v := validator.New()

	v.Required("email", i.Email)
	v.IsEmail("email", i.Email)

	v.Required("password", i.Password)

	return v.IsValid(), v.Errors
}
