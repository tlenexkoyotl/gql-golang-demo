package model

import (
	"time"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/tlenexkoyotl/meetmeup/jwt_secret"

	"golang.org/x/crypto/bcrypt"
)

type User struct {
	tableName struct{}   `pg:"users"`
	ID        string     `json:"id"`
	Username  string     `json:"username"`
	Email     string     `json:"email"`
	Password  string     `json:"Password"`
	FirstName string     `json:"firstName"`
	LastName  string     `json:"lastName"`
	CreatedAt time.Time  `json:"createdAt"`
	UpdatedAt time.Time  `json:"updatedAt"`
	DeletedAt *time.Time `json:"-" pg:",soft_delete"`
}

func (u *User) HashPassword(password string) error {
	bytePassword := []byte(password)
	passwordHash, err := bcrypt.GenerateFromPassword(bytePassword, bcrypt.DefaultCost)

	if err != nil {
		return err
	}

	u.Password = string(passwordHash)

	return nil
}

func (u *User) GenToken() (*AuthToken, error) {
	expiresAt := time.Now().Add(time.Hour * 24 * 7) // a week

	claims := jwt.StandardClaims{
		ExpiresAt: expiresAt.Unix(),
		Id:        u.ID,
		IssuedAt:  time.Now().Unix(),
		Issuer:    "meetmeup",
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	accesToken, err := token.SignedString(jwt_secret.GetSecret())

	if err != nil {
		return nil, err
	}

	authToken := &AuthToken{
		AccessToken: accesToken,
		ExpiredAt:   expiresAt,
	}

	return authToken, nil
}

func (u *User) ComparePassword(password string) error {
	bytePassword := []byte(password)
	byteHashedPassword := []byte(u.Password)

	return bcrypt.CompareHashAndPassword(byteHashedPassword, bytePassword)
}
