package graph

import (
	"context"

	"github.com/99designs/gqlgen/graphql"
	"github.com/vektah/gqlparser/v2/gqlerror"

	"gitlab.com/tlenexkoyotl/meetmeup/validator"
)

func validation(ctx context.Context, v validator.Validation) bool {
	isValid, errors := v.Validate()

	if !isValid {
		for k, e := range errors {
			graphql.AddError(ctx, &gqlerror.Error{
				Message:   e,
				Path:      nil,
				Locations: nil,
				Extensions: map[string]interface{}{
					"field": k,
				},
				Rule: "",
			})
		}
	}

	return isValid
}
