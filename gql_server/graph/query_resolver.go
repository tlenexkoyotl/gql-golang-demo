package graph

import (
	"context"

	"gitlab.com/tlenexkoyotl/meetmeup/graph/generated"
	"gitlab.com/tlenexkoyotl/meetmeup/graph/model"
)

type queryResolver struct{ *Resolver }

func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

func (r *queryResolver) User(ctx context.Context, id string) (*model.User, error) {
	return r.Domain.UserRepo.GetUserByID(id)
}

func (r *queryResolver) Meetups(ctx context.Context, filter *model.MeetupFilter, limit, offset *int) ([]*model.Meetup, error) {
	return r.Domain.MeetupRepo.GetMeetups(filter, limit, offset)
}

func (r *queryResolver) Users(ctx context.Context) ([]*model.User, error) {
	return r.Domain.UserRepo.GetUsers()
}
