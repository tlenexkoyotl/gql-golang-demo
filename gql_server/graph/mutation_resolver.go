package graph

import (
	"context"
	"errors"

	"gitlab.com/tlenexkoyotl/meetmeup/graph/generated"
	"gitlab.com/tlenexkoyotl/meetmeup/graph/model"
)

type mutationResolver struct{ *Resolver }

var (
	ErrInput = errors.New("Inputs error")
)

func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

func (r *mutationResolver) Register(ctx context.Context, input model.RegisterInput) (*model.AuthResponse, error) {
	isValid := validation(ctx, input)

	if !isValid {
		return nil, ErrInput
	}
	return r.Domain.Register(ctx, input)
}

func (r *mutationResolver) Login(ctx context.Context, input model.LoginInput) (*model.AuthResponse, error) {
	isValid := validation(ctx, input)

	if !isValid {
		return nil, ErrInput
	}

	return r.Domain.Login(ctx, input)
}

func (r *mutationResolver) CreateMeetup(ctx context.Context, input model.NewMeetup) (*model.Meetup, error) {
	return r.Domain.CreateMeetup(ctx, input)
}

func (r *mutationResolver) UpdateMeetup(ctx context.Context, id string, input model.UpdateMeetup) (*model.Meetup, error) {
	return r.Domain.UpdateMeetup(ctx, id, input)
}

func (r *mutationResolver) DeleteMeetup(ctx context.Context, id string) (bool, error) {
	return r.Domain.DeleteMeetup(ctx, id)
}
