package graph

import (
	"context"

	"gitlab.com/tlenexkoyotl/meetmeup/graph/generated"
	"gitlab.com/tlenexkoyotl/meetmeup/graph/model"
)

type userResolver struct{ *Resolver }

func (r *Resolver) User() generated.UserResolver { return &userResolver{r} }

func (r *userResolver) Meetups(ctx context.Context, obj *model.User) ([]*model.Meetup, error) {
	return r.Domain.MeetupRepo.GetMeetupsByUser(obj)
}
