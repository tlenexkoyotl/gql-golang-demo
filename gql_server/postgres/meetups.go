package postgres

import (
	"fmt"

	"github.com/go-pg/pg/v10"
	"gitlab.com/tlenexkoyotl/meetmeup/graph/model"
)

type MeetupRepo struct {
	DB *pg.DB
}

func (m *MeetupRepo) GetByID(id string) (*model.Meetup, error) {
	var meetup model.Meetup

	err := m.DB.Model(&meetup).Where("id = ?", id).First()

	return &meetup, err
}

func (m *MeetupRepo) GetMeetups(filter *model.MeetupFilter, limit, offset *int) ([]*model.Meetup, error) {
	var meetups []*model.Meetup

	query := m.DB.Model(&meetups).Order("id")

	if filter != nil {
		if filter.Name != nil && len(*filter.Name) > 0 {
			query = query.Where("name ILIKE ?", fmt.Sprintf("%%%s%%", *filter.Name))
		}
	}

	if limit != nil && *limit > 0 {
		query = query.Limit(*limit)
	}

	if offset != nil {
		query = query.Offset(*offset)
	}

	err := query.Select()

	if err != nil {
		return nil, err
	}

	return meetups, nil
}

func (m *MeetupRepo) GetMeetupsByUser(obj *model.User) ([]*model.Meetup, error) {
	var meetups []*model.Meetup

	err := m.DB.Model(&meetups).Where("user_id = ?", obj.ID).Order("id").Select()

	if err != nil {
		return nil, err
	}

	return meetups, nil
}

func (m *MeetupRepo) CreateMeetup(meetup *model.Meetup) (*model.Meetup, error) {
	_, err := m.DB.Model(meetup).Returning("*").Insert()

	return meetup, err
}

func (m *MeetupRepo) UpdateMeetup(meetup *model.Meetup) (*model.Meetup, error) {
	_, err := m.DB.Model(meetup).Where("id = ?", meetup.ID).Returning("*").Update()

	return meetup, err
}

func (m *MeetupRepo) DeleteMeetup(meetup *model.Meetup) error {
	_, err := m.DB.Model(meetup).Where("id = ?", meetup.ID).Delete()

	return err
}
