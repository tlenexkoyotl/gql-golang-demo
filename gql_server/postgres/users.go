package postgres

import (
	"fmt"

	"github.com/go-pg/pg/v10"
	"gitlab.com/tlenexkoyotl/meetmeup/graph/model"
)

type UserRepo struct {
	DB *pg.DB
}

func (u *UserRepo) CreateUser(tx *pg.Tx, user *model.User) (*model.User, error) {
	_, err := tx.Model(user).Returning("*").Insert()

	return user, err
}

func (u *UserRepo) GetUserByField(field, val string) (*model.User, error) {
	var user model.User
	field = fmt.Sprintf("%v = ?", field)

	err := u.DB.Model(&user).Where(field, val).First()

	return &user, err
}

func (u *UserRepo) GetUsers() ([]*model.User, error) {
	var users []*model.User

	err := u.DB.Model(&users).Select()

	return users, err
}

func (u *UserRepo) GetUserByID(id string) (*model.User, error) {
	return u.GetUserByField("id", id)
}

func (u *UserRepo) GetUserByEmail(email string) (*model.User, error) {
	return u.GetUserByField("email", email)
}

func (u *UserRepo) GetUserByUsername(username string) (*model.User, error) {
	return u.GetUserByField("username", username)
}
