ALTER TABLE users DROP COLUMN first_name,
    DROP COLUMN last_name,
    DROP COLUMN `password`,
    DROP COLUMN created_at,
    DROP COLUMN updated_at,
    DROP COLUMN deleted_at;