CREATE TABLE meetup (
    id bigserial PRIMARY KEY,
    name varchar(256) UNIQUE NOT NULL,
    description text,
    user_id bigserial REFERENCES users (id) ON DELETE CASCADE NOT NULL
);

