INSERT INTO users (
        id,
        username,
        email,
        first_name,
        last_name,
        "password",
        created_at,
        updated_at
    )
VALUES (
        1,
        'Lone Eagle',
        'lone_eagle@gmail.com',
        'Lone',
        'Eagle',
        '$2a$10$wSgzuLvIMUIcRj9.JQ.eUO / U.HkyyvCpWcOgBwPEyUCEMbkPYmfja',
        '2020-09-22T02:34:49Z',
        '2020-09-22T02:34:49Z'
    ),
    (
        2,
        'Crazy Horse',
        'crazy_horse@gmail.com',
        'Crazy',
        'Horse',
        '$2a$10$wSgzuLvIMUIcRj9.JQ.eUO / U.HkyyvCpWcOgBwPEyUCEMbkPYmfja',
        '2020-09-22T02:34:49Z',
        '2020-09-22T02:34:49Z'
    ),
    (
        3,
        'Fire Wolf',
        'fire_wolf@gmail.com',
        'Fire',
        'Wolf',
        '$2a$10$wSgzuLvIMUIcRj9.JQ.eUO / U.HkyyvCpWcOgBwPEyUCEMbkPYmfja',
        '2020-09-22T02:34:49Z',
        '2020-09-22T02:34:49Z'
    );
SELECT setval('users_id_seq', 3, true);
INSERT INTO meetup (id, name, description, user_id)
VALUES (
        1,
        'My first meetup ',
        'This is a description',
        1
    ),
    (
        2,
        'My second meetup ',
        'This is another description',
        1
    ),
    (
        3,
        'A third meetup ',
        'This is a 3rd description',
        2
    ),
    (4, 'A fourth meetup! ', 'A new description', 3);
SELECT setval('meetup_id_seq', 4, true);