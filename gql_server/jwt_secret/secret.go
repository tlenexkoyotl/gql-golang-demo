package jwt_secret

import "gitlab.com/tlenexkoyotl/meetmeup/env"

func GetSecret() []byte {
	return []byte(env.GetEnv("JWT_SECRET", "a_secret_huhu"))
}
