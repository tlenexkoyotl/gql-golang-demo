#!/bin/bash

TAG=v4.12.2
DIR=$HOME/.gvm/pkgsets/go1.15/testgo

go get -u -d github.com/golang-migrate/migrate/cmd/migrate
cd $DIR/src/github.com/golang-migrate/migrate/cmd/migrate
git checkout $TAG
go build \
    -tags 'postgres' \
    -ldflags="-X main.Version=$(git describe --tags)" \
    -o $DIR/bin/migrate \
    $DIR/src/github.com/golang-migrate/migrate/cmd/migrate
