package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/rs/cors"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/go-pg/pg/v10"
	"gitlab.com/tlenexkoyotl/meetmeup/auth_middleware"
	"gitlab.com/tlenexkoyotl/meetmeup/domain"
	"gitlab.com/tlenexkoyotl/meetmeup/env"
	"gitlab.com/tlenexkoyotl/meetmeup/graph"
	"gitlab.com/tlenexkoyotl/meetmeup/graph/generated"
	"gitlab.com/tlenexkoyotl/meetmeup/postgres"
)

func main() {
	DB := postgres.New(&pg.Options{
		User:     env.GetEnv("PG_USER", "dev"),
		Password: env.GetEnv("PG_PASSWORD", "d3v_s4f3_p4ssw0rd"),
		Database: env.GetEnv("PG_DATABASE", "meetmeup_dev"),
		Addr:     env.GetEnv("PG_HOST", "gql-golang-demo-db") + ":" + env.GetEnv("PG_PORT", "5432"),
	})

	defer DB.Close()

	DB.AddQueryHook(postgres.DBLogger{})

	gqlPort := env.GetEnv("GQL_PORT", "80")
	router := chi.NewRouter()

	corsOptions := cors.Options{
		AllowedOrigins:     []string{fmt.Sprintf("http://localhost:%s", gqlPort)},
		MaxAge:             0,
		AllowCredentials:   false,
		OptionsPassthrough: false,
		Debug:              true,
	}
	corsHandler := cors.New(corsOptions).Handler

	userRepo := postgres.UserRepo{DB: DB}

	router.Use(corsHandler)
	router.Use(middleware.RequestID)
	router.Use(middleware.Logger)
	router.Use(auth_middleware.AuthMiddleware(userRepo))

	domain := domain.NewDomain(userRepo, postgres.MeetupRepo{DB: DB})
	resolver := &graph.Resolver{Domain: domain}

	c := generated.Config{Resolvers: resolver}

	srv := handler.NewDefaultServer(generated.NewExecutableSchema(c))

	router.Handle("/", playground.Handler("GraphQL playground", "/query"))
	router.Handle("/query", graph.DataloaderMiddleware(DB, srv))

	log.Printf("connect to http://localhost:%s/ for GraphQL playground", gqlPort)
	log.Fatal(http.ListenAndServe(":"+gqlPort, router))
}
