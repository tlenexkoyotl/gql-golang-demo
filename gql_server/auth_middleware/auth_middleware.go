package auth_middleware

import (
	"context"
	"net/http"
	"strings"

	"gitlab.com/tlenexkoyotl/meetmeup/graph/model"
	"gitlab.com/tlenexkoyotl/meetmeup/jwt_secret"
	"gitlab.com/tlenexkoyotl/meetmeup/postgres"

	"github.com/dgrijalva/jwt-go/request"
	"github.com/pkg/errors"

	"github.com/dgrijalva/jwt-go"
)

const CurrentUserKey = "currentKey"

func AuthMiddleware(repo postgres.UserRepo) func(http.Handler) http.Handler {
	httpHandler := func(next http.Handler) http.Handler {
		httpHandlerFunc := func(w http.ResponseWriter, r *http.Request) {
			token, err := parseToken(r)

			if err != nil {
				next.ServeHTTP(w, r)
				return
			}

			claims, ok := token.Claims.(jwt.MapClaims)

			if !(ok && token.Valid) {
				next.ServeHTTP(w, r)
				return
			}

			user, err := repo.GetUserByID(claims["jti"].(string))

			if err != nil {
				next.ServeHTTP(w, r)
				return
			}

			ctx := context.WithValue(r.Context(), CurrentUserKey, user)

			next.ServeHTTP(w, r.WithContext(ctx))
		}

		return http.HandlerFunc(httpHandlerFunc)
	}

	return httpHandler
}

func stripBearerPrefixFromToken(token string) (string, error) {
	bearer := "BEARER"

	if len(token) > len(bearer) && strings.ToUpper(token[0:len(bearer)]) == bearer {
		return token[len(bearer)+1:], nil
	}

	return token, nil
}

var authHeaderExtractor = &request.PostExtractionFilter{
	Extractor: request.HeaderExtractor{"Authorization"},
	Filter:    stripBearerPrefixFromToken,
}

var authExtractor = &request.MultiExtractor{
	authHeaderExtractor,
	request.ArgumentExtractor{"access_token"},
}

func parseToken(r *http.Request) (*jwt.Token, error) {
	keyFunc := func(token *jwt.Token) (interface{}, error) {
		t := jwt_secret.GetSecret()

		return t, nil
	}

	jwtToken, err := request.ParseFromRequest(r, authExtractor, keyFunc)

	return jwtToken, errors.Wrap(err, "parseToken error")
}

func GetCurrentUserFromCtx(ctx context.Context) (*model.User, error) {
	err := errors.New("No user in context")

	if ctx.Value(CurrentUserKey) == nil {
		return nil, err
	}

	user, ok := ctx.Value(CurrentUserKey).(*model.User)

	if !ok || user.ID == "" {
		return nil, err
	}

	return user, nil
}
