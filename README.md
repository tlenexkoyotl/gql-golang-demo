# gql-golang-demo

<div style="text-align: center;">
<img src="https://raw.githubusercontent.com/graph-gophers/graphql-go/master/docs/img/logo.png" alt="drawing" style="width:10vw;"/>
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/Postgresql_elephant.svg/500px-Postgresql_elephant.svg.png" alt="drawing" style="width:8vw;"/>
</div>

This is an example showing how to easily build a `GraphQL` server using `golang`, `gqlgen`, `go-pg`, `dataloaden` and `PostgreSQL`. Most of what is shown here can also be very useful in building a `GraphQL` server even with a different data layer.

## Set up a PostgreSQL DB
This repository contains an example [`dockerfile`](https://gitlab.com/tlenexkoyotl/gql-golang-demo/-/blob/master/postgres_db/Dockerfile.dev) showing how to set up a `PostgreSQL 12.4` DB server, including how to define a custom `user`, `password` and `database` within the image, as well as allowing remote connections.

This repository also contains an example [`docker-compose.yml`](https://gitlab.com/tlenexkoyotl/gql-golang-demo/-/blob/master/docker-compose.yml) showing how to properly setup the `PostgreSQL` DB server for data and settings persistence, as well as passing arguments to the image during build-time.

## Initializing a gqlgen server

In order to initialize a `GraphQL` server using `gqlgen` a `go module` must first be initialized:

```bash
user@pc:~/gql-golang-demo/gql_server$ ls -a
. ..
# Inside an empty folder execute:
# e.g.
user@pc:~/gql-golang-demo/gql_server$ REPO_PATH=gitlab.com/tlenexkoyotl/meetmeup
user@pc:~/gql-golang-demo/gql_server$ go module init $REPO_PATH

```
This command will create a fresh `go.mod` file for dependencies to be added into:

Obtain `gqlgen` dependency
```bash
user@pc:~/gql-golang-demo/gql_server$ go get github.com/99designs/gqlgen
```

Generate `GraphQL` server boilerplate using `gqlgen`

```bash
user@pc:~/gql-golang-demo/gql_server$ go run github.com/99designs/gqlgen init
```

A folder structure as follows is then created:

```bash
gql_server
|
|__graph
|  |
|  |___generated
|  |  |
|  |  |__generated.go
|  |
|  |___model
|  |  |
|  |  |__models_gen.go
|  |
|  |___resolver.go
|  |
|  |___schema.graphqls
|  |
|  |___schema.resolvers.go
|
|__go.mod
|__go.sum
|__gqlgen.yml
|__server.go
```

The file `resolver.go` must be modified as such:
```go
package graph

//go:generate go run github.com/99designs/gqlgen

type Resolver struct {}

```

Given that `gqlgen` is a schema-first library, the first file to be modified to suit our schema needs is `schema.graphqls` and `schema.resolvers.go` must be deleted (will be regenerated), afterwards boilerplate code can be regeneratd using the directive added to `resolver.go` as so:

```bash
user@pc:~/gql-golang-demo/gql_server$ cd graph
user@pc:~/gql-golang-demo/gql_server/graph$ go generate
```

It is a good practice to move each of the generated models in `graph/model/models_gen.go` into its own file.

It can be useful to setup custom resolvers for certain fields in the `gqlgen.yml` file:
```yml
models:
  User:
    fields:
      meetups:
        resolver: true
  Meetup:
    fields:
      user:
        resolver: true
```

That is enough to get the boilerplate working.

## Dockerizing the GraphQL server
This repository contains an example [`dockerfile`](https://gitlab.com/tlenexkoyotl/gql-golang-demo/-/blob/master/gql_server/Dockerfile.dev) showing how to set up the `GraphQL` API, including how to install its dependencies.

As mentioned above, this repository contains the proper [`docker-compose.yml`](https://gitlab.com/tlenexkoyotl/gql-golang-demo/-/blob/master/docker-compose.yml) showing how to properly setup the `GraphQL` API using compose and the afore-mentioned `dockerfile`.

It can be very useful to use a file similar to [`run_app.sh`](https://gitlab.com/tlenexkoyotl/gql-golang-demo/-/blob/master/run_app.sh) for easier use of `docker-compose` commands.

## Set up go-pg
Setting up the very powerful `go-pg` ORM is very simple, first `github.com/go-pg/pg/v10` dependency must be added:

```bash
user@pc:~/gql-golang-demo/gql_server$ go get github.com/go-pg/pg/v10
```

Repo structs must be created, compositing with a pointer to a `pg.DB` type, which enables ORM use within the Repo methods ([see](https://gitlab.com/tlenexkoyotl/gql-golang-demo/-/commit/6a15b986c8acd2573b9c9073d707af057c68a10d)).

### Migrations
[`golang-migrate`](https://github.com/golang-migrate/migrate/) CLI is a very useful tool to keep track of database versions properly. This repository includes a [script that allows for an easy installation](https://gitlab.com/tlenexkoyotl/gql-golang-demo/-/commit/a95e40f04c6b92309f3b8274bf6fa6e8f06abdd6) of `golang-migrate`.

Migrations can then be created using the following command:

```bash
user@pc:~/gql-golang-demo/gql_server$ MIGRATION_NAME=create_meetups
user@pc:~/gql-golang-demo/gql_server$ MIGRATIONS_DIR=postgres/migrations
user@pc:~/gql-golang-demo/gql_server$ migrate create -ext sql -dir $MIGRATIONS_DIR $MIGRATION_NAME
```
Migrations can be applied using the following command:

```bash
user@pc:~/gql-golang-demo/gql_server$ PG_USER=dev
user@pc:~/gql-golang-demo/gql_server$ PG_PASSWORD=d3v_s4f3_p4ssw0rd
user@pc:~/gql-golang-demo/gql_server$ PG_HOST=localhost
user@pc:~/gql-golang-demo/gql_server$ PG_PORT=5432
user@pc:~/gql-golang-demo/gql_server$ PG_DATABASE=meetmeup_dev
user@pc:~/gql-golang-demo/gql_server$ PG_URL="postgres://$PG_USER:$PG_PASSWORD@localhost:$PG_PORT/$PG_DATABASE?sslmode=disable" 
user@pc:~/gql-golang-demo/gql_server$ MIGRATIONS_DIR=postgres/migrations
user@pc:~/gql-golang-demo/gql_server$ migrate -path $MIGRATIONS_DIR -database "$PG_URL" up
```

[Custom table names can be indicated](https://gitlab.com/tlenexkoyotl/gql-golang-demo/-/commit/df8f3b97eb86281fc666ab51c813bc27f92291ed) to `go-pg` using the ``tableName   struct{} `pg:"meetup"` ``.

It can be useful to create a [`DBLogger` hook](https://gitlab.com/tlenexkoyotl/gql-golang-demo/-/commit/a1a198e5562d53a5325fcebffdaba46cb9af02b0), this allows for query visualization in the terminal.

[To make use of repos in resolvers, they must be injected into the root resolver](https://gitlab.com/tlenexkoyotl/gql-golang-demo/-/commit/d771f30d03c8ca2f2b58643deffe403a24454428).

Finally, the query hook can be injected into a database connection as it can be created in [`server.go`](https://gitlab.com/tlenexkoyotl/gql-golang-demo/-/commit/da744a26047fb4e2115b4460f7dcc10e0e534af2) and injected into the Repos.

## Adding Dataloader support

First, a model loader must be generated:

```bash
user@pc:~/gql-golang-demo/gql_server/graph$ LOADER_NAME=UserLoader
user@pc:~/gql-golang-demo/gql_server/graph$ LOADER_TYPE=string
user@pc:~/gql-golang-demo/gql_server/graph$ MODEL='*gitlab.com/tlenexkoyotl/meetmeup/graph/model.User'
user@pc:~/gql-golang-demo/gql_server/graph$ go run github.com/vektah/dataloaden $LOADER_NAME $LOADER_TYPE $MODEL
```

[Then the loader can be employed](https://gitlab.com/tlenexkoyotl/gql-golang-demo/-/commit/a77cfa55525e9a1a6ac46a6b299cfcd20ec48ef1), (to prevent data disorganization use [this fix](https://gitlab.com/tlenexkoyotl/gql-golang-demo/-/commit/74dfa0bcc34471e1762255559ce16eb1f6f03fd0)).

Finally, in [`server.go`](https://gitlab.com/tlenexkoyotl/gql-golang-demo/-/commit/28c9ff547e56fbf87bf62c1d816dbd14bd3ac3f4) the `DataloaderMiddleware` is employed.
